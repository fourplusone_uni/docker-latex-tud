FROM ubuntu
MAINTAINER Matthias Bartelmeß <bartelmess@stud.tu-darmstadt.de>

ADD debian-tuddesign-keyring.gpg .
RUN apt-key add debian-tuddesign-keyring.gpg

RUN echo "deb http://exp1.fkp.physik.tu-darmstadt.de/tuddesign/ lenny tud-design" >> /etc/apt/sources.list
RUN echo "deb-src http://exp1.fkp.physik.tu-darmstadt.de/tuddesign/ lenny tud-design" >> /etc/apt/sources.list
RUN apt-get update -y
RUN apt-get install -y t1-tudfonts tex-tudfonts ttf-tudfonts latex-tuddesign latex-tuddesign-thesis texlive-full

WORKDIR /opt/texio
